*** Settings ***
Documentation    "Account creation" page-object.
Library    Browser
Resource    ../../browser_helpers/checkbox_radiobutton_helpers.resource


*** Variables ***
${HOME_PAGE_NAME}    PrestaShop
&{ACC_LOCATORS}    page_title=Login
...                title=//h1[contains(text(), "Créez votre compte")]
...                mister_rb=//*[@id="field-id_gender-1"]
...                miss_rb=//*[@id="field-id_gender-2"]
...                first_name_field=//*[@id="field-firstname"]
...                last_name_field=//*[@id="field-lastname"]
...                email_field=//*[@id="field-email"]
...                password_field=//*[@id="field-password"]
...                birth_date_field=//*[@id="field-birthday"]
...                privacy_cb=//*[@name="customer_privacy"]
...                gdpr_cb=//*[@name="psgdpr"]
...                news_cb=//*[@name="newsletter"]
...                offers_cb=//*[@name="optin"]
...                validate_btn=//*[@id="customer-form"]/footer/button


*** Keywords ***
Fill AccountCreation Form
    [Arguments]    ${gender}    ${first}    ${last}    ${password}    ${mail}
    ...            ${birth}    ${offers}    ${privacy}    ${news}    ${gdpr}
    [Documentation]    Fills the account creation form with given parameters.

    &{gender_M} =    Create Dictionary    label=M    rb=${ACC_LOCATORS}[mister_rb]
    &{gender_F} =    Create Dictionary    label=F    rb=${ACC_LOCATORS}[miss_rb]
    @{genders} =    Create List    ${gender_M}    ${gender_F}

    Select Radio Button Regarding Choice    ${gender}    @{genders}

    Fill Text    ${ACC_LOCATORS}[first_name_field]    ${first}
    Fill Text    ${ACC_LOCATORS}[last_name_field]    ${last}
    Fill Text    ${ACC_LOCATORS}[email_field]    ${mail}
    Fill Text    ${ACC_LOCATORS}[password_field]    ${password}
    Fill Text    ${ACC_LOCATORS}[birth_date_field]    ${birth}

    Select Checkbox Regarding Choice  ${offers}  ${ACC_LOCATORS}[offers_cb]
    Select Checkbox Regarding Choice  ${privacy}  ${ACC_LOCATORS}[privacy_cb]
    Select Checkbox Regarding Choice  ${news}  ${ACC_LOCATORS}[news_cb]
    Select Checkbox Regarding Choice  ${gdpr}  ${ACC_LOCATORS}[gdpr_cb]

Submit AccountCreation Form
    [Documentation]    Initiates account creation.
    Click    ${ACC_LOCATORS}[validate_btn]

Page Should Be AccountCreation Page
    [Documentation]    Checks that the current page is the "account creation" page.
    Get Title    ==    ${ACC_LOCATORS}[page_title]
    Get Element    ${ACC_LOCATORS}[title]

Store Email And Password After Account Creation
    [Arguments]    ${email}    ${password}
    [Documentation]    Stores email and password after account creation has succeeded.
    ${page} =    Get Title
    IF    "${page}" == "${HOME_PAGE_NAME}"
        Set Test Variable    ${OLD_PASSWORD}    ${password}
        Set Test Variable    ${USER_MAIL}    ${email}
    END
