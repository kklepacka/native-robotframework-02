*** Settings ***
Documentation    "Identity" page-object.
Library    String
Library    DateTime
Library    Browser
Resource    ../../browser_helpers/checkbox_radiobutton_helpers.resource


*** Variables ***
&{ID_LOCATORS}    page_title=Identity
...               title=//h1[contains(text(), "Vos informations personnelles")]
...               mister_rb=//*[@id="field-id_gender-1"]
...               miss_rb=//*[@id="field-id_gender-2"]
...               first_name_field=//*[@id="field-firstname"]
...               last_name_field=//*[@id="field-lastname"]
...               email_field=//*[@id="field-email"]
...               birth_date_field=//*[@id="field-birthday"]
...               submit_button=//*[@id="customer-form"]/footer/button
...               privacy_cb=//*[@name="customer_privacy"]
...               gdpr_cb=//*[@name="psgdpr"]
...               news_cb=//*[@name="newsletter"]
...               offers_cb=//*[@name="optin"]
...               old_password_field=//*[@id="field-password"]
...               new_password_field=//*[@id="field-new_password"]
...               after_modif_msg=//*[@class="alert alert-success"]
...               success_msg=Information mise à jour avec succès.


*** Keywords ***
Fill MyIdentity Form
    [Arguments]    ${gender}    ${first}    ${last}    ${password}    ${mail}
    ...            ${birth}    ${offers}    ${privacy}    ${news}    ${gdpr}
    [Documentation]    Fills the my identity form.

    &{gender_M} =    Create Dictionary    label=M    rb=${ID_LOCATORS}[mister_rb]
    &{gender_F} =    Create Dictionary    label=F    rb=${ID_LOCATORS}[miss_rb]
    @{genders} =    Create List    ${gender_M}    ${gender_F}

    Select Radio Button Regarding Choice    ${gender}    @{genders}

    Fill Text    ${ID_LOCATORS}[first_name_field]    ${first}
    Fill Text    ${ID_LOCATORS}[last_name_field]    ${last}
    Fill Text    ${ID_LOCATORS}[email_field]    ${mail}
    Fill Text    ${ID_LOCATORS}[new_password_field]    ${password}
    Fill Text    ${ID_LOCATORS}[birth_date_field]    ${birth}
    Fill Text    ${ID_LOCATORS}[old_password_field]    ${OLD_PASSWORD}

    Select Checkbox Regarding Choice  ${offers}  ${ID_LOCATORS}[offers_cb]
    Select Checkbox Regarding Choice  ${privacy}  ${ID_LOCATORS}[privacy_cb]
    Select Checkbox Regarding Choice  ${news}  ${ID_LOCATORS}[news_cb]
    Select Checkbox Regarding Choice  ${gdpr}  ${ID_LOCATORS}[gdpr_cb]

Submit MyIdentity Form
    [Documentation]    Initiates account information modification.
    Click    ${ID_LOCATORS}[submit_button]

Private Information Should Be
    [Arguments]    ${gender}    ${first}    ${last}    ${mail}    ${birth}
    [Documentation]    Checks that the specified values match the personal information of a logged user.

    ${value_attribute} =    Set Variable    value

    &{gender_M} =    Create Dictionary    label=M    rb=${ID_LOCATORS}[mister_rb]
    &{gender_F} =    Create Dictionary    label=F    rb=${ID_LOCATORS}[miss_rb]
    @{genders} =    Create List    ${gender_M}    ${gender_F}

    Verify Selected Radio Button    ${gender}     @{genders}

    Get Attribute    ${ID_LOCATORS}[first_name_field]    ${value_attribute}    ==    ${first}
    Get Attribute    ${ID_LOCATORS}[last_name_field]    ${value_attribute}    ==    ${last}
    Get Attribute    ${ID_LOCATORS}[email_field]    ${value_attribute}    ==    ${mail}
    Get Attribute    ${ID_LOCATORS}[birth_date_field]    ${value_attribute}    ==    ${birth}

Page Should Be MyIdentity Page
    [Documentation]    Checks that the current page is the "my identity" page.
    Get Title    ==    ${ID_LOCATORS}[page_title]
    Get Element    ${ID_LOCATORS}[title]

Change Email To Random
    [Arguments]    ${user_pwd}
    [Documentation]    Changes email of a logged user to <TIMESTAMP>_<random_string>@newmail.com.

    ${rand_str} =    Generate Random String
    ${curr_date} =    Get Current Date    result_format=%Y%m%d_%H%M%S%f
    ${new_mail} =    Set Variable    ${curr_date}_${rand_str}@newmail.com

    Fill Text    ${ID_LOCATORS}[email_field]    ${new_mail}
    Fill Text    ${ID_LOCATORS}[old_password_field]    ${user_pwd}
    Check Checkbox    ${ID_LOCATORS}[privacy_cb]
    Check Checkbox    ${ID_LOCATORS}[gdpr_cb]
    Click    ${ID_LOCATORS}[submit_button]

    Email Have Been Successfully Changed To    ${new_mail}

Email Have Been Successfully Changed To
    [Arguments]    ${email}
    [Documentation]    Checks that the email have successfully been changed to the given parameter.
    Get Attribute    ${ID_LOCATORS}[after_modif_msg]       data-alert    ==    success
    Get Text    ${ID_LOCATORS}[after_modif_msg]/ul/li    ==    ${ID_LOCATORS}[success_msg]
    Get Attribute    ${ID_LOCATORS}[email_field]    value    ==    ${email}

Store Email And Password After Account Modification
    [Arguments]    ${email}    ${password}
    [Documentation]    Manages email and password storage for teardown.
    Get Title    ==    ${ID_LOCATORS}[page_title]
    @{msg_states} =    Get Element States   ${ID_LOCATORS}[after_modif_msg]
    ${list_length} =    Get Length    ${msg_states}
    IF    ${list_length} > 1
        Set Test Variable    $OLD_PASSWORD    ${password}
        Set Test Variable    $USER_MAIL    ${email}
    END
